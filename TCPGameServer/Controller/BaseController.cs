﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCPGameServer.Controller
{
    abstract class BaseController
    {
        protected RequestCode requestCode = RequestCode.None;

        public RequestCode RequestCode
        {
            get { return requestCode; }
        }

        /// <summary>
        /// 返回值是返回给客户端的内容
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual string DefaultHandle(string data) { return null; }
    }
}
